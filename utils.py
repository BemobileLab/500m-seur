from dexml.fields import Value
import datetime


class Datetime(Value):
    """Field representing a datetime value."""
    def parse_value(self, val):
        return datetime.datetime.strptime(val, '%Y-%m-%d %H:%M:%S.%f')