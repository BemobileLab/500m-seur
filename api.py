# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import requests
from exceptions import APIError, DecodeError
from requests.exceptions import ConnectionError, Timeout
import xml.etree.ElementTree as ET
import xmltodict
try:
    import urllib.parse as urlrequest
except ImportError:
    import urllib as urlrequest


class HttpAPI(object):

    def request(self, url, data=None, **params):
        method = 'POST'
        params = params or {}

        headers = {
            'Content-Type': 'text/plain'
        }

        encoded_params = urlrequest.urlencode(params)
        url = self._absolute_url(url, encoded_params)

        try:
            result = requests.post(url, data=data.encode('utf-8'), headers=headers)
        except ConnectionError as e:
            msg = '{}'.format(e)

            if msg:
                msg = '%s: %s' % (type(e).__name__, msg)
            else:
                msg = type(e).__name__

            raise APIError(msg)

        except Timeout as e:
            msg = '{}'.format(e)

            if msg:
                msg = '%s: %s' % (type(e).__name__, msg)
            else:
                msg = type(e).__name__

            raise APIError(msg)

        if result.status_code not in (requests.codes.ok, requests.codes.not_found,
                                      requests.codes.created, requests.codes.accepted,
                                      requests.codes.no_content):
            self._create_apierror(result, url=url, data=data, method=method)
        elif result.status_code == requests.codes.no_content or result.status_code == requests.codes.not_found:
            return result, None
        else:
            if result.content:
                try:
                    content = xmltodict.parse(ET.fromstring(result.content.encode('utf-8'))[0][0][0].text)
                    if 'ERROR' in content:
                        self._create_apierror(result, url=url, data=content, method=method)
                    return result, content
                except ValueError:

                    try:
                        content = xmltodict.parse(result.content)
                        if 'ERROR' in content:
                            self._create_apierror(result, url=url, data=content, method=method)
                        return result, content
                    except ValueError:
                        self._create_decodeerror(result, url=url)
            else:
                self._create_decodeerror(result, url=url)

    def _absolute_url(self, url, encoded_params):
        from . import Config

        pattern = '%s%s%s'

        if encoded_params:
            pattern = '%s%s?%s'

        return pattern % (Config.base_url, url, encoded_params)

    def _create_apierror(self, result, url=None, data=None, method=None):
        text = result.text if hasattr(result, 'text') else result.content
        status_code = result.status_code
        headers = result.headers

        if status_code < 500:
            try:
                content = xmltodict.parse(ET.fromstring(text)[0][0][0].text)
                error = content['ERROR'].get('DESCRIPCION')
                if not error:
                    error = 'No error description. Code: ' + content['ERROR'].get('CODIGO')
            except Exception as e:
                error = str(result)

        else:
            try:
                error = ET.fromstring(text)[0][0][1].text
            except Exception as e:
                error = str(result)
        raise APIError(error, code=status_code, content=result, headers=headers, url=url)

    def _create_decodeerror(self, result, url=None):
        text = result.text if hasattr(result, 'text') else result.content
        status_code = result.status_code
        headers = result.headers

        raise DecodeError(text, code=status_code, headers=headers, content=text, url=url)
