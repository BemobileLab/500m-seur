import xmltodict
from base import BaseApiModel
from dexml import fields
from api import HttpAPI
from utils import Datetime
from exceptions import NetworkError
import datetime


class Address(BaseApiModel):
    street_type = fields.String(tagname='tipoVia__variableName', required=False)
    street = fields.String(tagname='calle__variableName')
    number_type = fields.String(tagname='tipoNumero__variableName', required=False)
    number = fields.String(tagname='numero__variableName')
    stairs = fields.String(tagname='escalera__variableName', required=False)
    floor = fields.String(tagname='piso__variableName', required=False)
    door = fields.String(tagname='puerta__variableName', required=False)
    postal_code = fields.String(tagname='codigoPostal__variableName')
    city = fields.String(tagname='poblacion__variableName')
    province = fields.String(tagname='provincia__variableName')
    country = fields.String(tagname='pais__variableName')

    def save(self):
        raise NotImplementedError()


class Person(BaseApiModel):
    legal_name = fields.String(tagname='razonSocial__variableName', required=False)
    name = fields.String(tagname='nombreContacto__variableName')
    last_name = fields.String(tagname='apellidosContacto__variableName')
    phone_prefix = fields.Integer(tagname='prefijoTelefono__variableName')
    phone_number = fields.Integer(tagname='telefono__variableName')
    legal_number = fields.String(tagname='nif__variableName', required=False)
    email = fields.String(tagname='mail__variableName', required=False)
    language = fields.String(tagname='idiomaContacto__variableName', required=False)
    address = fields.Model(Address)

    def save(self):
        raise NotImplementedError()


class Company(Person):

    def as_xml(self, exclude=None):
        from . import Config
        self.clean()
        xml = super(Company, self).as_xml()
        xml = self._complete_xml_defaults(xml)
        ccc = '<cccOrdenante>%s</cccOrdenante>\n' % Config.client_ccc
        return self._credentials + xml + '%s' + ccc

    def _complete_xml_defaults(self, xml):
        from . import Config
        xml = xml.replace('__variableName', 'Ordenante').replace('razonSocialOrdenante', 'razonSocial')
        company_name = '<nombreEmpresa>%s</nombreEmpresa>' % Config.company.legal_name
        xml = xml.replace('</razonSocial>', '</razonSocial>' + company_name)
        xml = xml.replace('</telefonoOrdenante>', '</telefonoOrdenante><prefijoFaxOrdenante /><faxOrdenante />')
        company_country = '<paisNifOrdenante>%s</paisNifOrdenante>' % Config.company.address.country
        xml = xml.replace('</nifOrdenante>', '</nifOrdenante>' + company_country)
        return xml

    def clean(self):
        if self.address and not self.address.street_type:
            self.address.street_type = 'PZA'
        if self.address and not self.address.number_type:
            self.address.number_type = 'N.'

    @property
    def _credentials(self):
        from . import Config
        usr = '<usuario>%s</usuario>\n' % Config.client_id
        password = '<password>%s</password>\n' % Config.client_pass
        return usr + password


class Status(BaseApiModel):
    name = fields.String()
    date = fields.String()


class Shipment(BaseApiModel):
    pickup_datetime = Datetime(required=False)
    _pickup_day = fields.Integer(tagname='diaRecogida')
    _pickup_month = fields.Integer(tagname='mesRecogida')
    _pickup_year = fields.Integer(tagname='anioRecogida')
    _am_from = fields.String(tagname='horaMananaDe', required=False)
    _am_to = fields.String(tagname='horaMananaA', required=False)
    number_of_packages = fields.Integer(tagname='numeroBultos')
    _pm_from = fields.String(tagname='horaTardeDe', required=False)
    _pm_to = fields.String(tagname='horaTardeA', required=False)
    observations = fields.String(tagname='observaciones', required=False)
    receiver = fields.Model(Person)
    sender = fields.Model(Person)
    value = fields.Float(tagname='valorDeclarado')
    reference = fields.String(tagname='numeroReferencia')
    locator = fields.String(tagname='localizador', required=False)
    status_history = fields.List(fields.Model(Status), required=False)
    fee = fields.Float(required=False)

    class Meta:
        requests = {
            'get_pickup': """
                <soap-env:Envelope xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/">
                    <soap-env:Body>
                        <ns0:consultaDetallesRecogidasStr xmlns:ns0="http://consultaRecogidas.servicios.webseur">
                            <ns0:in0>%s</ns0:in0>
                            <ns0:in1></ns0:in1>
                            <ns0:in2>%s</ns0:in2>
                            <ns0:in3>%s</ns0:in3>
                        </ns0:consultaDetallesRecogidasStr>
                    </soap-env:Body>
                </soap-env:Envelope>""",
            'get_delivery': """
                <soap-env:Envelope xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/">
                    <soap-env:Body>
                        <ns0:consultaExpedicionesRelStr xmlns:ns0="http://consultaRecogidas.servicios.webseur">
                            <ns0:in0>%s</ns0:in0>
                            <ns0:in1>%s</ns0:in1>
                            <ns0:in2>%s</ns0:in2>
                        </ns0:consultaExpedicionesRelStr>
                    </soap-env:Body>
                </soap-env:Envelope>"""
        }

    @property
    def status(self):
        if filter(lambda x: x.name.lower() == 'entrega efectuada', self.status_history):
            return 'Delivered'
        if filter(lambda x: x.name.lower() == 'pick up done', self.status_history):
            return 'Picked'
        if filter(lambda x: x.name.lower() == 'created in system pick up order', self.status_history) or self.locator:
            return 'Created'
        return None

    @property
    def history(self):
        return map(lambda x: x.name, self.status_history)

    @property
    def _auth(self):
        from . import Config
        company = Config.company
        return company.as_xml()

    def save(self):
        self.clean()
        if self.locator:
            raise NotImplementedError('Object is immutable. Cant update it')

        data = self.as_xml()
        api = HttpAPI()
        url = '/WSCrearRecogida'

        _, data = api.request(url, data=data)
        if not data:
            raise NetworkError()

        result = self.parse_result(data)
        setattr(self, 'fee', float(result['recogida']['tasacion']))
        setattr(self, 'locator', result['recogida']['localizador'])

    @classmethod
    def get(cls, locator):
        pickup = cls._get_pickup(locator)
        if pickup:
            reference, status_history = cls._get_status_history(pickup)

            delivery = cls._get_delivery(locator)
            if delivery:
                status_history.append(Status(
                    date=delivery['fecha_ultima_situacion'],
                    name=delivery['descripcion_para_cliente']))

            if reference:
                return cls(locator=locator, status_history=status_history, reference=reference)
        return None

    def as_xml(self, exclude=None):
        self.clean()
        result = ''
        replaceable_index = 0
        skip = False
        reference_number = ''
        for obj in self.irender(fragment=True):
            if 'Shipment>' in obj or 'pickup_datetime' in obj:
                continue

            if '<Person>' in obj:
                if replaceable_index == 0:
                    obj = self.receiver.as_xml(exclude=['prefijoTelefono__variableName']).replace('__variableName', 'Destino')+'\n'
                    obj += '<prefijoTelefonoDestino>%s</prefijoTelefonoDestino>' % self.receiver.phone_prefix
                    replaceable_index += 1
                else:
                    obj = self.sender.as_xml(exclude=['prefijoTelefono__variableName']).replace('__variableName', 'Origen')
                    obj = obj.replace('telefonoOrigen', 'telefonoRecogidaOrigen') + '\n'
                    obj += '<prefijoTelefonoOrigen>%s</prefijoTelefonoOrigen>\n' % self.sender.phone_prefix
                    replaceable_index += 1

                result += obj
                skip = True

            elif '</Person>' in obj:
                skip = False
                continue

            if skip:
                continue

            if 'numeroReferencia>' in obj:
                reference_number = obj + '\n'
                continue

            result += (obj + '\n')

        part_1, part_2 = self._complete_xml_defaults(result)
        result = (self._auth % part_1) + reference_number + part_2
        return self._request_wraper() % result

    def _complete_xml_defaults(self, xml):
        from . import Config
        xml = xml.replace('</anioRecogida>', '''</anioRecogida>
            <servicio>1</servicio>
         ''')
        xml = xml.replace('</numeroBultos>', '''</numeroBultos>
            <mercancia>2</mercancia>
        ''')

        if 'horaMananaDe' not in xml:
            xml = xml.replace('</servicio>', '''</servicio>
                <horaMananaDe />
                <horaMananaA />
             ''')
        if 'horaTardeDe' not in xml:
            xml = xml.replace('</mercancia>', '''</mercancia>
                <horaTardeDe />
                <horaTardeA />
                <tipoPorte>Q</tipoPorte>
             ''')
        else:
            xml = xml.replace('</horaTardeA>', '''</horaTardeA>
                <tipoPorte>Q</tipoPorte>
             ''')

        if '</observaciones>' in xml:
            xml = xml.replace('</observaciones>', ('''</observaciones>
                <tipoAviso>E</tipoAviso>
                <idiomaContactoOrdenante>%s</idiomaContactoOrdenante>
            ''') % Config.company.address.country)
        elif '<observaciones />' in xml:
            xml = xml.replace('<observaciones />', ('''<observaciones />
                <tipoAviso>E</tipoAviso>
                <idiomaContactoOrdenante>%s</idiomaContactoOrdenante>
            ''') % Config.company.address.country)
        else:
            xml = xml.replace('</tipoPorte>', ('''</tipoPorte>
                <observaciones>-</observaciones>
                <tipoAviso>E</tipoAviso>
                <idiomaContactoOrdenante>%s</idiomaContactoOrdenante>
            ''') % Config.company.address.country)

        xml = xml.replace('</prefijoTelefonoOrigen>', '''</prefijoTelefonoOrigen>
            <producto>2</producto>
            <entregaSabado>N</entregaSabado>
            <entregaNave>N</entregaNave>
            <tipoEnvio>N</tipoEnvio>
        ''')

        xml = xml.replace('</valorDeclarado>', '''</valorDeclarado>
            <listaBultos>1;1;1;1;1/</listaBultos>
        ''')

        part_2 = '''
            <ultimaRecogidaDia />
            <nifOrigen>B00000000</nifOrigen>
            <paisNifOrigen>%s</paisNifOrigen>
            <aviso>N</aviso>
            <cccDonde />
            <cccAdonde />
            <tipoRecogida>R</tipoRecogida>
            <scValorAsegurado>0</scValorAsegurado>
            <scSeurPlus></scSeurPlus>
            <scValorReembolso>0</scValorReembolso>
            <scComprobanteEntregaTipo></scComprobanteEntregaTipo>
            <scEntregaIdentificador></scEntregaIdentificador>
        ''' % Config.company.address.country
        return xml, part_2

    def clean(self):
        if self.receiver and self.receiver.address and not self.receiver.address.street_type:
            self.receiver.address.street_type = 'CL'
        if self.sender and self.sender.address and not self.sender.address.street_type:
            self.sender.address.street_type = 'CL'
        if self.receiver and self.receiver.address and not self.receiver.address.number_type:
            self.receiver.address.number_type = 'N.'
        if self.sender and self.sender.address and not self.sender.address.number_type:
            self.sender.address.number_type = 'N.'

        self.sender.legal_name = self.sender.name + ' ' + self.sender.last_name
        self.receiver.legal_name = self.receiver.name + ' ' + self.receiver.last_name
        self.receiver.legal_number = None
        self.receiver.email = None
        self.receiver.language = None
        self.sender.legal_number = None
        self.sender.email = None
        self.sender.language = None

        pickup_day = str(self.pickup_datetime.day)
        self._pickup_day = ('0' + pickup_day) if len(pickup_day) < 2 else pickup_day
        pickup_month = str(self.pickup_datetime.month)
        self._pickup_month = ('0' + pickup_month) if len(pickup_month) < 2 else pickup_month
        pickup_year = str(self.pickup_datetime.year)
        self._pickup_year = int('20' + pickup_year) if len(pickup_year) == 2 else pickup_year
        self.number_of_packages = ('0' + str(self.number_of_packages)) if len(str(self.number_of_packages)) < 2 else self.number_of_packages

        if self.pickup_datetime.hour < 13:
            self._am_from = '10:00'
            self._am_to = '13:00'
        else:
            self._pm_from = '16:00'
            self._pm_to = '18:00'

        if not self.value:
            self.value = 0

    def _request_wraper(self):
        return """
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cre="http://crearRecogida.servicios.webseur">
           <soapenv:Header/>
           <soapenv:Body>
              <cre:crearRecogida>
                 <cre:in0><![CDATA[<?xml version="1.0" encoding="ISO-8859-1"?>
        <recogida>
            %s\n
        </recogida>]]></cre:in0>
              </cre:crearRecogida>
           </soapenv:Body>
        </soapenv:Envelope>
        """

    @classmethod
    def _get_pickup(cls, locator):
        from . import Config
        data = cls.Meta.requests['get_pickup'] % (locator, Config.client_id, Config.client_pass)
        api = HttpAPI()
        url = '/WSConsultaRecogidas'

        _, data = api.request(url, data=data)
        if not data or 'soap:Envelope' not in data:
            return None

        data = data['soap:Envelope']['soap:Body']['ns1:consultaDetallesRecogidasStrResponse']['ns1:out']
        data = xmltodict.parse(data.encode('utf-8').lower())
        result = data.get('recogidas', {}).get('recogida')
        return result

    @classmethod
    def _get_delivery(cls, locator):
        from . import Config
        data = cls.Meta.requests['get_delivery'] % (locator, Config.client_id, Config.client_pass)
        api = HttpAPI()
        url = '/WSConsultaRecogidas'

        _, data = api.request(url, data=data)
        if not data or 'soap:Envelope' not in data:
            return None

        data = data['soap:Envelope']['soap:Body']['ns1:consultaExpedicionesRelStrResponse']['ns1:out']
        data = xmltodict.parse(data.encode('utf-8').lower())

        result = data.get('expediciones', {}).get('expedicion')
        if result and isinstance(result, list):
            result = result[0]
        return result


    @staticmethod
    def _get_status_history(pickup):
        status_history = []
        reference = None
        if pickup and pickup.get('num_referencia'):
            reference = pickup['num_referencia']

            situations = pickup.get('situaciones', {})
            if not situations:
                return reference, status_history
            situation = situations.get('situacion', [])
            if not situation:
                return reference, status_history

            if not isinstance(situation, list):
                situation = [situation]

            for s in situation:
                status_history.append(Status(date=s['fecha_situacion'], name=s['descripcion_cliente_ingles']))
            status_history.sort(key=lambda status: datetime.datetime.strptime(status.date, "%d-%m-%Y %H:%M:%S"))

        return reference, status_history

