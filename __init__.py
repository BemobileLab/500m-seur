from resources import Company, Address


class Config:
    base_url = ''
    client_id = ''
    client_pass = ''
    client_ci = ''
    client_ccc = ''
    company = None
    debug = False


class Seur(object):
    def __init__(self):
        self.config = Config

    def init_app(self, app):
        self.config.client_id = app.config['SEUR_SETTINGS']['ID']
        self.config.client_pass = app.config['SEUR_SETTINGS']['PASS']
        self.config.client_ci = app.config['SEUR_SETTINGS']['CLIENT_CI']
        self.config.client_ccc = app.config['SEUR_SETTINGS']['CLIENT_CCC']
        self.config.base_url = app.config['SEUR_SETTINGS']['HOST']
        self.config.debug = app.config['DEBUG']

        company_details = app.config['SEUR_SETTINGS']['COMPANY_DETAILS']
        company_address = Address()
        company_address.street = company_details['ADDRESS']['STREET']
        company_address.number = company_details['ADDRESS']['NUMBER']
        company_address.postal_code = company_details['ADDRESS']['POSTAL_CODE']
        company_address.city = company_details['ADDRESS']['CITY']
        company_address.province = company_details['ADDRESS']['PROVINCE']
        company_address.country = company_details['ADDRESS']['COUNTRY']
        company = Company()
        company.legal_name = company_details['LEGAL_NAME']
        company.name = company_details['NAME']
        company.last_name = company_details['LAST_NAME']
        company.phone_prefix = company_details['PHONE_PREFIX']
        company.phone_number = company_details['PHONE_NUMBER']
        company.legal_number = company_details['LEGAL_NUMBER']
        company.email = company_details['EMAIL']
        company.address = company_address
        self.config.company = company

